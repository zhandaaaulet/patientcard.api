﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PatientCard.Core.Dtos;
using PatientCard.Core.Entities;
using PatientCard.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace PatientCard.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientCardController : ControllerBase
    {
        private readonly IPatientCardRepository _patientCardRepository;
        private readonly IMapper _mapper;

        public PatientCardController(IPatientCardRepository patientCardRepository, IMapper mapper)
        {
            _patientCardRepository = patientCardRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPatients()
        {
            var patients = await _patientCardRepository.GetAllAsync();
            var patientsToReturn = _mapper.Map<IEnumerable<PatientDto>>(patients);
            return patients != null ? (IActionResult)Ok(patientsToReturn) : NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> CreatePatient(PatientCreateDto createDto)
        {
            var patient = new Patient()
            {
                IIN = createDto.IIN,
                Fullname = createDto.Fullname,
                Address = createDto.Address,
                Number = createDto.Number,
                Diagnosis = createDto.Diagnosis,
                Complaints = createDto.Complaints,
                DateOfVisit = createDto.DateOfVisit,
                Specialist = createDto.Specialist
            };
            await _patientCardRepository.AddAsync(patient);
            return Ok(patient.Id);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPatientById(int id)
        {
            var patient = await _patientCardRepository.GetByIdAsync(id);
            var patientToSend = _mapper.Map<PatientDto>(patient);
            return patient != null ? (IActionResult)Ok(patientToSend) : NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePatient(int id)
        {
            var patient = await _patientCardRepository.GetByIdAsync(id);
            await _patientCardRepository.DeleteAsync(patient);
            return Ok("Removed successfully!");
        }

        [HttpPut]
        [ProducesResponseType(typeof(Patient), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdatePatient([FromBody] Patient p)
        {
            await _patientCardRepository.UpdateAsync(p);
            return Ok("Updated successfuly");
        }
    }
}

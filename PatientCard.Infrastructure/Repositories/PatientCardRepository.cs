﻿using Microsoft.EntityFrameworkCore;
using PatientCard.Core.Entities;
using PatientCard.Core.Interfaces.Repositories;
using PatientCard.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatientCard.Infrastructure.Repositories
{
    public class PatientCardRepository : EntityRepository<Patient>, IPatientCardRepository
    {
        private readonly DbSet<Patient> _patients;

        public PatientCardRepository(DataContext dbContext) : base(dbContext)
        {
            _patients = dbContext.Patients; // or dbContext.Set<Product>()
        }
    }
}

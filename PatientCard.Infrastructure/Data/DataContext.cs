﻿using Microsoft.EntityFrameworkCore;
using PatientCard.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatientCard.Infrastructure.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Specialist> Specialists { get; set; }

    }
}

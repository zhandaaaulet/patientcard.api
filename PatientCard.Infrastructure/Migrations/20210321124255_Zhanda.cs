﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PatientCard.Infrastructure.Migrations
{
    public partial class Zhanda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Complaints",
                table: "Patients",
                type: "varchar(45)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DateOfVisit",
                table: "Patients",
                type: "varchar(45)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Diagnosis",
                table: "Patients",
                type: "varchar(45)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SpecialistId",
                table: "Patients",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Specialists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Fullname = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specialists", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Patients_SpecialistId",
                table: "Patients",
                column: "SpecialistId");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Specialists_SpecialistId",
                table: "Patients",
                column: "SpecialistId",
                principalTable: "Specialists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Specialists_SpecialistId",
                table: "Patients");

            migrationBuilder.DropTable(
                name: "Specialists");

            migrationBuilder.DropIndex(
                name: "IX_Patients_SpecialistId",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "Complaints",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "DateOfVisit",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "Diagnosis",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "SpecialistId",
                table: "Patients");
        }
    }
}

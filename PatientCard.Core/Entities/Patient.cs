﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PatientCard.Core.Entities
{
    public class Patient
    {
        public int Id { get; set; }

        [Column(TypeName = "varchar(16)")]
        public string IIN { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string Fullname { get; set; }

        [Column(TypeName = "varchar(45)")]
        public string Address { get; set; }

        [Column(TypeName = "varchar(11)")]
        public string Number { get; set; }

        [Column(TypeName = "varchar(45)")]
        public string Diagnosis { get; set; }

        [Column(TypeName = "varchar(45)")]
        public string Complaints { get; set; }

        [Column(TypeName = "varchar(45)")]
        public string DateOfVisit { get; set; }

        public Specialist Specialist { get; set; }

    }
}

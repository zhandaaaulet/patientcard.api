﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatientCard.Core.Entities
{
    public class Specialist
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
    }
}

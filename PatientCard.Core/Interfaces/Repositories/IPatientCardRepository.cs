﻿using PatientCard.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatientCard.Core.Interfaces.Repositories
{
    public interface IPatientCardRepository : IEntityRepository<Patient>
    {
    }
}

﻿using PatientCard.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PatientCard.Core.Dtos
{
    public class PatientCreateDto
    {
        [Required]
        [StringLength(255)]
        public string IIN { get; set; }
        public string Fullname { get; set; }
        public string Address { get; set; }
        public string Number { get; set; }
        public string Diagnosis { get; set; }
        public string Complaints { get; set; }
        public string DateOfVisit { get; set; }
        public Specialist Specialist { get; set; }
    }
}

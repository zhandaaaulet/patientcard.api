﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatientCard.Core.Dtos
{
    public class PatientDto
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Number { get; set; }
    }
}

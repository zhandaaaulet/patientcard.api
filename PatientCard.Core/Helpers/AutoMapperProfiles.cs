﻿using AutoMapper;
using PatientCard.Core.Dtos;
using PatientCard.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatientCard.Core.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Patient, PatientDto>();
        }
    }
}
